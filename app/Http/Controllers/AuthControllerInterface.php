<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

interface AuthControllerInterface
{
    /**
     * @param Request $request
     * @return ResponseFactory|Response
     */
    public function register(Request $request);

    /**
     * @param Request $request
     * @return ResponseFactory|Response
     */
    public function login(Request $request);

    /**
     * @param Request $request
     * @return ResponseFactory|Response
     */
    public function logout(Request $request);
}
