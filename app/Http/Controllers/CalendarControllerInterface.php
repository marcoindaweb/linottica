<?php

namespace App\Http\Controllers;

use App\Events;
use Illuminate\Http\Request;
use App\Calendar;

interface CalendarControllerInterface
{
    /**
     * @return Calendar[]
     */
    public function getAllCalendars():iterable;

    /**
     * @param int $id
     * @return Calendar
     */
    public function getCalendar(int $id): Calendar;

    /**
     * @param int $id
     * @return Events[]
     */
    public function getCalendarEvents(int $id);
}
