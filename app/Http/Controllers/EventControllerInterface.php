<?php

namespace App\Http\Controllers;

use App\Events;
use Illuminate\Http\Request;

interface EventControllerInterface
{
    /**
     * @return Events[]
     */
    public function getAllEvents():iterable;

    /**
     * @param int $id
     * @return Events
     */
    public function getEvent(int $id): Events;

    /**
     * @param Request $request
     * @return boolean
     */
    public function addEvent(Request $request);

    /**
     * @param int $id
     * @param Request $request
     * @return mixed
     */
    public function updateEvent(int $id, Request $request);

    /**
     * @param int $id
     */
    public function deleteEvent(int $id);
}
