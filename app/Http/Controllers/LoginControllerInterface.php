<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

interface LoginControllerInterface
{
    public function login(Request $request):bool ;
}
