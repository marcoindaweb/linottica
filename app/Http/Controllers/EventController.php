<?php

namespace App\Http\Controllers;

use App\Events;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;

class EventController extends Controller implements EventControllerInterface
{
    public function getAllEvents(): iterable
    {
        return Events::all();
    }

    public function getEvent(int $id): Events
    {
        return Events::find($id);
    }

    public function addEvent(Request $request)
    {
        return Events::create($request->all());
    }

    public function updateEvent(int $id, Request $request)
    {
        $event = Events::findOrFail($id);
        $event->update($request->all());

        return $event;
    }

    public function deleteEvent(int $id)
    {
        $event = Events::findOrFail($id);
        $event->delete();
        //  NO CONTENT
        return 204;
    }
}
