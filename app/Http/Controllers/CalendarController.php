<?php

namespace App\Http\Controllers;

use App\Calendar;
use Illuminate\Http\Request;

class CalendarController extends Controller implements CalendarControllerInterface
{
    public function getAllCalendars(): iterable
    {
        return Calendar::all();
    }

    public function getCalendar(int $id): Calendar
    {
        return Calendar::find($id);
    }

    public function getCalendarEvents(int $id)
    {
        return Calendar::find($id)->events;
    }
}
