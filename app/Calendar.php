<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $fillable = [
        'name',
        'address',
        'color',
        'activated'         //  if true, calendar is open on start
    ];

    /**
     * Get the events for the current calendar.
     */
    public function events()
    {
        return $this->hasMany('App\Events');
    }
}
