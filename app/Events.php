<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $fillable = [
        'title',
        'description',
        'calendar_id',
        'schedule',
        'icon',
        'location'
    ];

    /**
     * Get the post that owns the comment.
     */
    public function calendar()
    {
        return $this->belongsTo('App\Calendar');
    }
}
