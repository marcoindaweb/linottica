
if [ "$MODE" = "dev" ]
then
  npm run dev
elif [ "$MODE" = "prod" ]
then
  npm run prod
else
  echo "NUXT MODE IS NOT VALID [$MODE]"
fi
