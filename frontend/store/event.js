
export const state = () => ({
  events: []
});

export const mutations = {
  FETCH_ALL_EVENTS(state, events) {
    state.events = events
  },
  CREATE_EVENT(state, events) {
    state.events = events
  },
  UPDATE_EVENT(state, events) {
    state.events = events
  },
  REMOVE_EVENT(state, events) {
    state.events = events
  }
}
export const actions = {
  FETCH_ALL_EVENTS({commit, rootState}) {
    return new Promise((resolve, reject) => {
      // Do something here... lets say, a http call using vue-resource
      this.$axios.get('events')
        .then(({data}) => {
          let events = []
          data.forEach((event) => {
            console.log(event)
            let schedule = JSON.parse(event.schedule)
            console.log(schedule)
            let calendarName = null
            let eventColor = null
            rootState.surgeries.surgeries.forEach((calendar) => {
              if(calendar.id === event.calendar_id) {
                calendarName = calendar.name
                eventColor = calendar.color
              }
            })
            events.push({
              data: {
                id: event.id,
                title: event.title,
                description: event.description,
                color: eventColor,
                calendar: calendarName
              },
              schedule: schedule
            })
          })
          events.forEach((event) => {
            if(event.data.color === null || event.data.color === undefined) {
              event.data.color = '#89CDEB'
            }
          })
          commit('FETCH_ALL_EVENTS', events)
          resolve(events)
      }, error => {
        reject(error);
      })
    })
  },
  UPDATE_EVENT({commit}, data) {
    commit('UPDATE_EVENT', data)
  },
  CREATE_EVENT({commit, dispatch, rootState}, data) {
    let event = {}
    event.title = data.details.title
    event.description = data.details.description
    let calendarName = data.details.calendar
    let calendarId = null
    rootState.surgeries.surgeries.forEach((calendar) => {
      if(calendar.name === calendarName) {
        calendarId = calendar.id
      }
    })
    event.calendar_id = calendarId + 1

    console.log(data.schedule)
    let schedule = {}
    Object.keys(data.schedule).forEach(function (item) {

      //  PER QUELLI CHE HANNO VALORE OBBLIGATORIO
      if(
        item === 'times' ||
        item === 'duration' ||
        item === 'durationUnit' ||
        item === 'durationInDays' ||
        item === 'checks' ||
        item === 'lastTime'
      ) {
        schedule[item] = data.schedule[item]
      }
      //  PER QUELLI CHE HANNO VALORE FACOLTATIVO
      if(
        item === 'year' ||
        item === 'month' ||
        item === 'week' ||
        item === 'weekOfYear' ||
        item === 'weekspanOfYear' ||
        item === 'fullWeekOfYear' ||
        item === 'lastWeekspanOfYear' ||
        item === 'lastFullWeekOfYear' ||
        item === 'weekOfMonth' ||
        item === 'weekspanOfMonth' ||
        item === 'fullWeekOfMonth' ||
        item === 'lastWeekspanOfMonth' ||
        item === 'lastFullWeekOfMonth' ||
        item === 'dayOfWeek' ||
        item === 'dayOfMonth' ||
        item === 'lastDayOfMonth' ||
        item === 'dayOfYear'
      ) {
        if(data.schedule[item].given === true) {
          schedule[item] = data.schedule[item].input
        }
      }
    });
    event.schedule = JSON.stringify(schedule)
    this.$axios.post('events', event)
      .then((data) => {
        this.$toast.show(data.statusText,{icon: 'event'})
      })
    dispatch('FETCH_ALL_EVENTS')
  },
  //  EVENT ID = event.data.id
  REMOVE_EVENT({commit, dispatch}, id) {
    this.$axios.delete('events/' + id,)
    dispatch('FETCH_ALL_EVENTS')
  }
}
export const getters = {
  events: (state) => {
    return state.events
  },
  event: (state) => id => {
    return state.events[id]
  }
}
