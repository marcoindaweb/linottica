
export const state = () => ({
    surgeries: [
        {
            id: 0,
            name: 'Linottica Thiene 1',
            position: 'VIA 123908 address',
            activated: false
        },
        {
            id: 1,
            name: 'Linottica Thiene 2',
            position: 'VIA 1203981209 Address',
            activated: false
        },
        {
            id: 2,
            name: 'Linottica Bassano',
            position: 'VIA 102938192 Address',
            activated: false
        }
    ],
    selectedSurgeries: {}
});

export const mutations = {
    setSelectedSurgeries (state, surgeries) {
        state.selectedSurgeries = surgeries[0]
    }
}
export const actions = {
    // selectMenuCard ({ commit }, selectedElement) {
    //     commit('setSelectedId', selectedElement.id)
    // }
}
export const getters = {
    //     selectedId: (state) => {
    //         return state.selectedId
    //     }
    getSelectedSurgeries: (state) => {
        return state.selectedSurgeries
    }
}
