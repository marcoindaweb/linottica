import { Calendar } from 'dayspan'

export const state = () => ({
    surgeries: [
        // {
        //     id: 0,
        //     name: 'Linottica Thiene 1',
        //     location: 'VIA 123908 address',
        //     activated: true,
        //     // color: '#EA8C81',
        //     color: '#EE4000',
        //     Calendar: Calendar.months()
        // },
        // {
        //     id: 1,
        //     name: 'Linottica Thiene 2',
        //     location: 'VIA 1203981209 Address',
        //     activated: false,
        //     // color: '#111111',
        //     color: '#101010',
        //     Calendar: Calendar.months()
        // },
        // {
        //     id: 2,
        //     name: 'Linottica Bassano',
        //     location: 'VIA 102938192 Address',
        //     activated: false,
        //     // color: '#89CDEB',
        //     color: '#3399FF',
        //     Calendar: Calendar.months()
        // }
    ],
    selectedSurgery: null
});

export const mutations = {
    calendarChangeState (state, id) {
        state.surgeries[id].activated = !state.surgeries[id].activated
    },
    setSelectedSurgery (state, id) {
        state.selectedSurgery = id
    },
    setSurgeries (state, data) {
        data.forEach(calendar => {
            calendar.id = calendar.id -1
        })
        state.surgeries = data
    }
}
export const actions = {
    SET_SURGERY ({commit}, selectedId) {
        commit('setSelectedSurgery', selectedId)
    },
    FETCH_ALL_SURGERIES({commit}) {
      this.$axios.get('calendars')
        .then(({data}) => {
            commit('setSurgeries', data)
      })
    }
}
export const getters = {
    surgeries: (state) => {
        return state.surgeries
    },
    activeSurgeries: (state) => {
        let count = 0;
        state.surgeries.forEach((item) => {
            if(item.activated) count++
        })
        return count
    },
    selectedSurgery: (state) => {
        return state.selectedSurgery
    }
}
