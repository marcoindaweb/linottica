import plugin from '../assets/plugin'
// import component from '../assets/component'
// import defaults from '../assets/defaults'

import Vuetify from 'vuetify'

import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'dayspan-vuetify/dist/lib/dayspan-vuetify.min.css'

import Vue from 'vue'
import * as moment from 'moment'

Vue.use(Vuetify, {})

Vue.use(plugin)

// Vue.use(component)

// Vue.use(defaults)

moment.locale('it')

