import Vue from 'vue'
import Vuetify from 'vuetify'
import DaySpanVuetify from './plugin'

import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import './styles/app.scss'

import fr from './locales/fr'
import en from './locales/en'
import de from './locales/de'
import ca from './locales/ca'
import nl from './locales/nl'
import it from './locales/it'

// import 'moment/lang/fr'
// import 'moment/lang/ca'
// import 'moment/lang/nl'
import 'moment/locale/it'
import * as moment from 'moment'
moment.locale('it')

Vue.config.productionTip = false

Vue.use(Vuetify);

Vue.use(DaySpanVuetify,
{
  data: {
    locales: { en, fr, de, nl, ca, it }
    // locales: { it }
  },
  methods: {
    getDefaultEventColor()
    {
      return '#C11520';
    }
  }
});
