
import it from './it'

export default {
  'it': it,
  'it-IT': it
}

export const defaultLocale = it
