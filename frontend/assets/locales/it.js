
export default {
  promptLabels: {
    actionRemove:       'Sei sicuro/a di rimuovere questo evento?',
    actionExclude:      'Are you sure you want to remove this event occurrence?',
    actionCancel:       'Sei sicuro/a di cancellare questo evento?',
    actionUncancel:     'Are you sure you want to uncancel this event?',
    actionSetStart:     'Are you sure you want to set this occurrence as the first?',
    actionSetEnd:       'Are you sure you want to set this occurrence as the last?',
    actionMove:         'Sei sicuro/a di muovere questo evento?',
    actionInclude:      'Are you sure you want to add an event occurrence?',
    move:               'Sei sicuro/a di muovere questo evento?',
    toggleAllDay:       'Sei sicuro/a di cambiare l\'impostazione di \'tutto il giorno\'?',
    removeExistingTime: 'Sei sicuro/a di rimuovere gli eventi in questo orario?'
  },
  placeholder: {
    noTitle: '(Nessun titolo)'
  },
  patterns: {
    lastDay:        (day) => 'Ultimo giorno del mese',
    lastDayOfMonth: (day) => 'Ultimo giorno di ' + day.format('MMMM'),
    lastWeekday:    (day) => 'Ultimo ' + day.format('dddd') + ' di ' + day.format('MMMM')
  },
  colors: [
    { text: 'Rosso' },
    { text: 'Rosa' },
    { text: 'Viola' },
    { text: 'Viola scuro' },
    { text: 'Indigo' },
    { text: 'Blu' },
    { text: 'Glue' },
    { text: 'Blu chiaro' },
    { text: 'Ciano' },
    { text: 'Teal' },
    { text: 'Verde' },
    { text: 'Verde chiaro' },
    { text: 'Lime' },
    { text: 'Giallo' },
    { text: 'Ambra' },
    { text: 'Arancione' },
    { text: 'Arancione scuro' },
    { text: 'Marrone' },
    { text: 'Blue Gray' },
    { text: 'Grigio' },
    { text: 'Nero' }
  ],
  icons: [
    { text: 'Allarme' },
    { text: 'Stella' },
    { text: 'Amore' },
    { text: 'Azione' },
    { text: 'Assegnamento' },
    { text: 'Attenzione' },
    { text: 'Soldi' },
    { text: 'Carica' },
    { text: 'Casa' },
    { text: 'Gioco' },
    { text: 'Email' },
    { text: 'Telefono' },
    { text: 'Grafico' },
    { text: 'Bici' },
    { text: 'Viaggio' }
  ],
  defaults: {
    dsDay: {
      formats: {
        month:  'MMM'
      }
    },
    dsCalendarApp: {
      types: [
        { label: 'Giorno' },
        { label: 'Settimana' },
        { label: 'Mese' },
        { label: 'Anno' },
        { label: 'Schedulario' },
        { label: '6 giorni' }
      ],
      formats: {
        today: 'dddd, MMMM D',
        xs: 'MMM'
      },
      labels: {
        next: (type) => type ? type.label + ' Successiva' : 'Next',
        prev: (type) => type ? type.label + ' Precedente' : 'Previous',
        moveCancel: 'Torna indietro',
        moveSingleEvent: 'Muovi l\'evento',
        moveOccurrence: 'Muovi solo questo evento',
        moveAll: 'Muovi tutti gli eventi',
        moveDuplicate: 'Aggiungi questo evento',
        promptConfirm: 'Sí',
        promptCancel: 'No',
        today: 'VAI A OGGI'
      }
    },
    dsAgendaEvent: {
      formats: {
        firstLine:  'ddd',
        secondLine: 'MMM Do',
        start:      'dddd, MMMM D',
        time:       'h:mm a'
      },
      labels: {
        allDay:   'Tutto il giorno',
        options:  'Opzioni',
        close:    'Chiudi',
        day:      ['giorno', 'giorni'],
        days:     ['giorno', 'giorni'],
        minute:   ['minuto', 'minuti'],
        minutes:  ['minuto', 'minuti'],
        hour:     ['ora', 'ore'],
        hours:    ['ora', 'ore'],
        week:     ['settimana', 'settimane'],
        weeks:    ['settimana', 'settimane'],
        second:   ['secondo', 'secondi'],
        seconds:  ['secondo', 'secondi'],
        busy:     'Occupato',
        free:     'Libero'
      }
    },
    dsCalendarEventChip: {
      formats: {
        fullDay:          'ddd MMM Do YYYY',
        timed:            'ddd MMM Do YYYY'
      }
    },
    dsCalendarEventPopover: {
      formats: {
        start:    'dddd, MMMM D',
        time:     'h:mm a'
      },
      labels: {
        allDay:   'Tutto il giorno',
        options:  'Opzioni',
        close:    'Chiudi',
        day:      ['giorno', 'giorni'],
        days:     ['giorno', 'giorni'],
        minute:   ['minuto', 'minuti'],
        minutes:  ['minuto', 'minuti'],
        hour:     ['ora', 'ore'],
        hours:    ['ora', 'ore'],
        week:     ['settimana', 'settimane'],
        weeks:    ['settimana', 'settimane'],
        second:   ['secondo', 'secondi'],
        seconds:  ['secondo', 'secondi'],
        busy:     'Occupato',
        free:     'Libero'
      }
    },
    dsCalendarEventCreatePopover: {
      formats: {
        start:    'dddd, MMMM D',
        time:     'h:mm a'
      },
      labels: {
        title:    'Aggiungi un titolo',
        allDay:   'Tutto il giorno',
        close:    'Chiudi',
        save:     'Salva',
        day:      ['giorno', 'giorni'],
        days:     ['giorno', 'giorni'],
        minute:   ['minuto', 'minuti'],
        minutes:  ['minuto', 'minuti'],
        hour:     ['ora', 'ore'],
        hours:    ['ora', 'ore'],
        week:     ['settimana', 'settimane'],
        weeks:    ['settimana', 'settimane'],
        second:   ['secondo', 'secondi'],
        seconds:  ['secondo', 'secondi'],
        busy:     'Occupato',
        free:     'Libero',
        location: 'Aggiungi posizione',
        description: 'Aggiungi descrizione',
        calendar: 'Calendario',
      },
      busyOptions: [
        {text: 'Occupato'},
        {text: 'Libero'}
      ]
    },
    dsSchedule: {
      labels: {
        editCustom:   'Modifica'
      },
    },
    dsEvent: {
      labels: {
        moreActions:  'Altro...',
        cancel:       'Cancella le modifiche all\'evento',
        save:         'Salva',
        title:        'Titolo',
        exclusions:   'These are events or spans of time where a normally occurring event was excluded from the schedule. Events are excluded here if an event occurrence is moved.',
        inclusions:   'These are events or spans of time where events were added outside the normally occurring schedule. Events are added here if an event occurrence is moved.',
        cancelled:    'These are events or spans of time where events were cancelled.',
        edit:         'Modifica l\'evento',
        add:          'Aggiungi un evento',
        location:     'Aggiungi posizione',
        description:  'Aggiungi descrizione',
        calendar:     'Calendario',
        tabs: {
          details:    'Dettagli dell\'evento',
          forecast:   'Forecast',
          removed:    'Rimosso',
          added:      'Aggiunto',
          cancelled:  'Cancellato'
        }
      },
      busyOptions: [
        {text: 'Occupato'},
        {text: 'Libero'}
      ]
    },
    dsScheduleActions: {
      labels: {
        remove:     'Rimuovi questo evento',
        exclude:    'Rimuovi questo evento',
        cancel:     'Cancella questo evento',
        uncancel:   'Torna indietro',
        move:       'Muovi questo evento',
        include:    'Aggiungi un nuovo evento',
        setStart:   'Imposta come primo evento',
        setEnd:     'Imposta come ultimo evento',
        pickerOk:   'OK',
        pickerCancel:'Cancella'
      }
    },
    dsScheduleForecast: {
      labels: {
        prefix:     'Le previsioni mostrano i precedenti e i successivi',
        suffix:     'eventi entro un anno.'
      }
    },
    dsScheduleFrequencyDay: {
      labels: {
        type: 'Giorni'
      },
      options: [
        { text: 'Tutto il giorno' },
        { text: 'Nei prossimi giorni...' },
        { text: 'Tutti _ giorni iniziano il _' }
      ],
      types: [
        { text: 'Giorno del mese' },
        { text: 'L\'ultimo giorno del mese' },
        { text: 'Giorno dell\'anno' }
      ]
    },
    dsScheduleFrequencyDayOfWeek: {
      weekdays: ['Domenica', 'Lunedí', 'Martedí', 'Mercoledí', 'Giovedí', 'Venerdí', 'Sabato'],
      labels: {
        type: 'Giorni della settimana'
      },
      options: [
        { text: 'Un giorno della settimana' },
        { text: 'Nei prossimi giorni della settimana...' },
        { text: 'Tutti i _ weekday iniziano il _' },
        { text: 'Weekends' },
        { text: 'Weekdays' }
      ]
    },
    dsScheduleFrequencyMonth: {
      labels: {
        type: 'Mesi'
      },
      months: [
        'Gennaio',
        'Febbraio',
        'Marzo',
        'Aprile',
        'Maggio',
        'Giugno',
        'Luglio',
        'Agosto',
        'Settembre',
        'Ottobre',
        'Novembre',
        'Dicembre'
      ],
      options: [
        { text: 'Qualsiasi mese' },
        { text: 'Nei prossimi mesi...' },
        { text: 'Tutti i _ mesi iniziano il _' }
      ]
    },

    dsScheduleFrequencyWeek: {
      labels: {
        type: 'Settimane'
      },
      options: [
        { text: 'Qualsiasi settimana' },
        { text: 'Nelle prossime settimane...' },
        { text: 'Tutte le _ settimane iniziano il _' }
      ],
      types: [
        { text: 'Week of the month (first week has a Thursday)' },
        { text: 'Weekspan of the month (starts on first day of month)' },
        { text: 'Full week of the month (0th = the week before if any)' },
        { text: 'Last weekspan of the month (starts on last day of month)' },
        { text: 'Last full week of the month (0th = the week after if any)' },
        { text: 'Week of the year (first week has a Thursday)' },
        { text: 'Weekspan of the year (starts on first day of year)' },
        { text: 'Full week of the year (0th = the week before if any)' },
        { text: 'Last weekspan of the year (starts on last day of year)' },
        { text: 'Last full week of the year (0th = the week after if any)' }
      ]
    },

    dsScheduleFrequencyYear: {
      labels: {
        type: 'Anni'
      },
      options: [
        { text: 'Qualsiasi anno' },
        { text: 'Nei prossimi anni...' },
        { text: 'Tutti gli _ anni iniziano il _' }
      ]
    },

    dsScheduleSpan: {
      labels: {
        startless:  'Inizio',
        endless:    'Fine'
      },
      formats: {
        start:      'MMMM Do, YYYY',
        end:        'MMMM Do, YYYY'
      }
    },

    dsScheduleTime: {
      labels: {
        remove:     'Rimuovi',
        add:        'Aggiungi'
      }
    },

    dsScheduleTimes: {
      labels: {
        all:        'Tutto il giorno',
        minute:     'minuto',
        minutes:    'minuti',
        hour:       'ora',
        hours:      'ore',
        day:        'giorno',
        days:       'giorni',
        week:       'settimana',
        weeks:      'settimane',
        month:      'mese',
        months:     'mesi',
        second:     'secondo',
        seconds:    'secondi'
      }
    },

    dsScheduleType: {
      formats: {
        date:       'LL'
      }
    },

    dsScheduleTypeCustomDialog: {
      labels: {
        save:     'Salva',
        cancel:   'Cancella'
      }
    },

    dsWeekDayHeader: {
      formats: {
        weekday:    'ddd'
      }
    },

    dsWeeksView: {
      weekdays: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab']
    },

    dsDaysView: {
      hours: [
        '    ', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11',
        '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'
      ]
    },

    dsDayPicker: {
      weekdays: ['Domenica', 'Lunedí', 'Martedí', 'Mercoledí', 'Giovedí', 'Venerdí', 'Sabato'],
      labels: {
        prevMonth: 'Mese precedente',
        nextMonth: 'Mese successivo'
      }
    }
  }
}
