<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//  LOGIN
Route::group(['middleware' => ['json.response']], function () {

    Route::middleware('auth:api')->get('user', function (Request $request) {
        return $request->user();
    });

    // public routes
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');

    // private routes
    Route::middleware('auth:api')->group(function () {
        Route::get('logout', 'AuthController@logout');
    });

});
Route::group(['middleware' => ['auth:api', 'json.response']], function () {
    //  EVENTS
    Route::get('events', 'EventController@getAllEvents');
    Route::get('events/{id}', 'EventController@getEvent');
    Route::post('events', 'EventController@addEvent');
    Route::put('events/{id}', 'EventController@updateEvent');
    Route::delete('events/{id}', 'EventController@deleteEvent');

    //  CALENDARS
    Route::get('calendars', 'CalendarController@getAllCalendars');
    Route::get('calendars/{id}', 'CalendarController@getCalendar');
    Route::get('calendars/{id}/events', 'CalendarController@getCalendarEvents');

});
